# VLAB Tools
## client_service.py
This is intended to run on a server and will listen for new zeroconf services which have the `_serial-port._tcp.local` domain. If it finds a service a tty device gets created using [ttynvt](https://gitlab.com/lars-thrane-as/ttynvt) and a symlink for the device according to its nick name.

## serial_server_async.py
### Arguments
```
Automatically serves usb serial interfaces using rfc2217 and
zeroconf

options:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        The Path for the config file
  -v, --verbose         Run in verbose mode
  ```
### Config
A config file defines the nick names and debug options for a certain device. If the program does not find any nick names for a device it uses the automatically generated one.

## get_port_uid.py
Generates the uid for a port.
