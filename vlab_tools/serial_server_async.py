import os, os.path
import sys
import socket
import serial, serial.rfc2217
import threading
import pyudev
import logging
import asyncio
import tomli
import ifaddr
import argparse
import atexit

from zeroconf import IPVersion
from zeroconf.asyncio import AsyncServiceInfo, AsyncZeroconf

from asyncio import StreamReader, StreamWriter

from time import sleep

from .common import generate_uid


LOG = logging.getLogger("Serial Server")

LOG_FORMATTER = logging.Formatter("[%(name)s][%(levelname)s]: %(message)s")
LOG_HANDLER = logging.StreamHandler()

LOG_HANDLER.setLevel(logging.WARNING)
LOG_HANDLER.setFormatter(LOG_FORMATTER)

LOG.addHandler(LOG_HANDLER)
LOG.setLevel(logging.DEBUG)


class RFC2217_Server(threading.Thread):
    writer = None
    reader = None

    def __init__(self, serial_path: str, tcp_port: int, logger: logging.Logger):
        threading.Thread.__init__(self)
        self.tcp_port = tcp_port
        self.serial_path = serial_path

        self.log = logger

        self.ser_instance = serial.serial_for_url(serial_path, do_not_open=True)
        self.ser_instance.timeout = 3
        self.ser_instance.dtr = False
        self.ser_instance.rts = False
        self.ser_settings = self.ser_instance.get_settings()
        self.running = False

    def run(self) -> bool:
        """Starts the tcp server inside the new thread"""
        self.running = True
        self.async_loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.async_loop)
        asyncio.run(self.start_tcp_server())
        self.log.debug("Thread is done")
        return False

    async def start_tcp_server(self) -> None:
        """Creates and starts the async tcp server"""
        self.server = await asyncio.start_server(
            self.handle_connect, "0.0.0.0", self.tcp_port
        )
        try:
            await self.server.serve_forever()
        except asyncio.exceptions.CancelledError as ex:
            pass

    async def read_loop(self) -> None:
        """Looks for new data from the tcp server and writes it to the serial instance"""
        self.log.info("Reading loop started")
        while self.running:
            self.log.debug("Waiting for data from tcp server")
            data = await self.reader.read(1024)
            if not data:
                break
            self.log.debug(f"Writing to serial: {data}")
            self.ser_instance.write(b"".join(self.rfc_manager.filter(data)))

    async def write_loop(self) -> None:
        """Reads from the serial port and writes the escaped data to the tcp client"""
        self.log.info("Writing loop started")
        while self.running:
            try:
                if self.ser_instance.in_waiting == 0:
                    await asyncio.sleep(0.01)
                    continue
                data = self.ser_instance.read(self.ser_instance.in_waiting)
                if data:
                    self.log.debug(f"Writing to tcp server from serial: {data}")
                    self.writer.write(b"".join(self.rfc_manager.escape(data)))
                    await self.writer.drain()
            except OSError as ex:
                self.log.debug("Serial got disconnected")
                self.ser_instance.close()
                break

    async def check_modem_lines_loop(self) -> None:
        """Continually checks if the modem lines (dtr, dts, etc.) have changed"""
        while True:
            self.rfc_manager.check_modem_lines()
            await asyncio.sleep(0.1)

    def write(self, data: bytes) -> None:
        """Synchronious function for writing to the tcp client"""
        self.log.debug(f"RFC Manager writes: {data}")
        self.writer.write(data)

    async def handle_disconnect(self) -> None:
        """Closes the serial port and reapplies the old settings"""
        self.log.info("Client Disconnected")

        if self.ser_instance.is_open:
            self.ser_instance.apply_settings(self.ser_settings)
            self.ser_instance.close()
        elif self.running == False or not self.ser_instance.is_open:
            self.server.close()
            await self.server.wait_closed()
        try:
            self.writer.close()
            await self.writer.wait_closed()
        except:
            pass
        self.writer = None
        self.reader = None

    async def handle_connect(self, reader: StreamReader, writer: StreamWriter) -> None:
        """Opens the serial port and starts the read, write and modem loop"""
        self.log.info("Client connected")
        if self.writer:
            writer.close()
            return
        self.reader = reader
        self.writer = writer

        loop_tasks = []

        self.ser_instance.open()
        loop_tasks.append(asyncio.create_task(self.read_loop()))
        loop_tasks.append(asyncio.create_task(self.write_loop()))
        loop_tasks.append(asyncio.create_task(self.check_modem_lines_loop()))
        self.rfc_manager = serial.rfc2217.PortManager(
            self.ser_instance, self, logger=None
        )
        done, pending = await asyncio.wait(
            loop_tasks, return_when=asyncio.FIRST_COMPLETED
        )
        self.log.debug("One Task exited")
        for task in pending:
            task.cancel()
        await self.handle_disconnect()


class ZeroAdvertiser:
    udev_context = pyudev.Context()

    serial_port_services = {}
    port_map = {}
    port_server_map = {}

    tcp_ports_free = []

    tcp_port_start = 7000
    tcp_port_end = 7020

    zeroconf = None
    loop = None

    def __init__(self, conf_file: str):
        """Loads the configuration"""
        self.toml_fd = open(conf_file, "rb")
        self.toml = tomli.load(self.toml_fd)
        self.addresses = self.get_all_ips()

    def get_all_ips(self) -> list[bytes]:
        """Loops through all interface which are not 'lo' and returns the corresponding ips as a byte representation"""
        ips = []
        adapters = ifaddr.get_adapters()
        for adapter in adapters:
            for ip in adapter.ips:
                if ip.nice_name == "lo":
                    continue
                if ip.is_IPv6:
                    ips.append(socket.inet_pton(socket.AF_INET6, ip.ip[0]))
                else:
                    ips.append(socket.inet_aton(ip.ip))
        return ips

    async def start(self) -> None:
        """Creates and starts the Zeroconf server according to the configuration toml file"""
        LOG.info("Starting Zeroconf server")
        self.zeroconf = await self.create_zeroconf()
        self.port_map = await self.check_for_ports()
        self.tcp_ports_free = await self.generate_tcp_ports()
        self.loop = asyncio.get_event_loop()

        tasks = []
        for port_id in self.port_map:
            port_cfg = self.toml.get(port_id, dict())
            LOG.debug(f"Port config: {port_cfg}")
            server = await self.create_server(self.port_map[port_id], port_id, port_cfg)
            service = await self.advertise_add(port_id, server.tcp_port, port_cfg)
            self.serial_port_services[server] = service
            self.port_server_map[port_id] = server
            server.start()

        while True:
            for server in self.serial_port_services:
                if not server.is_alive():
                    LOG.debug("Thread closed")
                    await self.advertiser_remove(server)
                    break
            await asyncio.sleep(0.1)

    async def generate_tcp_ports(self) -> list[int]:
        """Generates the valid tcp port range"""
        return list(range(self.tcp_port_start, self.tcp_port_end))

    async def create_server(
        self, port: pyudev.Device, port_id: str, port_cfg: logging.Logger
    ) -> RFC2217_Server:
        """Creates the RFC2217 Server and creates new logging instances"""
        handler = logging.StreamHandler()

        handler.setFormatter(LOG_FORMATTER)
        logger = logging.getLogger(port_id)
        logger.addHandler(handler)

        if port_cfg.get("debug", False):
            handler.setLevel(logging.DEBUG)
            logger.setLevel(logging.DEBUG)
        return RFC2217_Server(port.device_node, self.tcp_ports_free.pop(0), logger)

    async def advertiser_remove(self, rfc_server: RFC2217_Server) -> None:
        """Removes the advertiser for a port"""
        LOG.debug(f"Removing advertiser for {rfc_server.serial_path}")
        self.tcp_ports_free.append(rfc_server.tcp_port)
        await self.zeroconf.async_unregister_service(
            self.serial_port_services[rfc_server]
        )
        self.serial_port_services.pop(rfc_server)

    async def advertise_add(
        self, port_name: str, service_port: int, port_cfg: dict
    ) -> AsyncServiceInfo:
        """Adds the advertiser for a port"""
        LOG.debug(f"Advertising {port_name}")
        service = AsyncServiceInfo(
            "_serial-port._tcp.local.",
            f"{port_name}._serial-port._tcp.local.",
            addresses=self.addresses,
            port=service_port,
            properties={
                "spec": "rfc2217",
                "nick_name": port_cfg.get("name", port_name),
            },
            server="0.0.0.0",
        )
        await self.zeroconf.async_register_service(service)
        return service

    async def create_zeroconf(self) -> AsyncZeroconf:
        """Creates the main Zeroconf server"""
        return AsyncZeroconf(ip_version=IPVersion.All)

    async def check_for_ports(self) -> dict[str, pyudev.Device]:
        """Looks for connected usb serial ports using pyudev"""
        valid_ports = {}
        for port in self.udev_context.list_devices(subsystem="tty"):
            if port.find_parent(subsystem="usb") != None:
                valid_ports[generate_uid(port)] = port
        return valid_ports

    async def async_cleanup(self) -> None:
        """Removes all advertisers and stops all servers"""
        tasks = [
            self.advertiser_remove(server)
            for server in self.serial_port_services.keys()
        ]
        LOG.debug(f"Closing tasks: {tasks}")
        background_tasks = await asyncio.gather(*tasks)
        await self.zeroconf.async_close()

    def cleanup(self) -> None:
        """Sync version of async_cleanup for use with atexit"""
        if self.loop:  # Ignore when nothing is started
            self.loop.run_until_complete(self.async_cleanup())


async def async_main(advertiser: ZeroAdvertiser):
    await advertiser.start()


def main():
    parser = argparse.ArgumentParser(
        prog=sys.argv[0],
        description="Automatically serves usb serial interfaces using rfc2217 and zeroconf",
    )
    parser.add_argument(
        "-c", "--config", required=True, help="The Path for the config file", type=str
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Run in verbose mode",
        action="store_true",
        default=False,
    )

    args = parser.parse_args()

    if args.verbose:
        LOG_HANDLER.setLevel(logging.DEBUG)

    LOG.debug(f"Got args: {args}")
    try:
        advertiser = ZeroAdvertiser(args.config)
        atexit.register(advertiser.cleanup)
    except Exception as ex:
        LOG.error(f"Config file {args.config} can't be opened")
        LOG.debug(f"Error: {ex}")
        sys.exit(1)
    loop = asyncio.get_event_loop()
    loop.create_task(async_main(advertiser))
    loop.run_forever()


if __name__ == "__main__":
    main()
