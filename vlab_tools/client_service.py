import threading
import socket
import subprocess
import atexit
import os, os.path
import signal
import logging
import argparse
import sys

from zeroconf import ServiceBrowser, ServiceListener, Zeroconf
from icmplib import multiping
from time import sleep

LOG = logging.getLogger("Client Service")

LOG_FORMATTER = logging.Formatter("[%(name)s][%(levelname)s]: %(message)s")
LOG_HANDLER = logging.StreamHandler()

LOG_HANDLER.setLevel(logging.WARNING)
LOG_HANDLER.setFormatter(LOG_FORMATTER)

LOG.addHandler(LOG_HANDLER)
LOG.setLevel(logging.WARNING)


class ZeroconfService(ServiceListener):
    """Zeroconf Service listener"""

    thread_map = {}
    available_ports = list(range(0, 1024))
    links = []
    name_map = {}
    is_creating = threading.Lock()

    def get_link_name(self, port_name: str) -> str:
        return f"/dev/tty-remote-{port_name}"

    def ttynvt_thread(
        self, port_name: str, tcp_port: str, host: str
    ) -> subprocess.Popen:
        """Creates a new subprocess for ttynvt and creates a symlink to the interface"""
        self.is_creating.acquire()

        tty_port = self.available_ports.pop()

        process = subprocess.Popen(
            ["ttynvt", "-d", "-n", f"ttyNVT{tty_port}", "-S", f"{host}:{tcp_port}"]
        )

        link_name = self.get_link_name(port_name)
        if os.path.lexists(link_name):
            LOG.debug(f"Removing {link_name}")
            os.remove(link_name)

        LOG.debug(f"Creating {link_name}")
        os.symlink(f"/dev/ttyNVT{tty_port}", link_name)
        self.links.append(link_name)

        self.is_creating.release()

        return process

    def update_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        """Callback which removes and creates a new ttynvt subprocess"""
        LOG.debug(f"Updating service: {name}")
        self.remove_service(zc, type_, name)
        self.add_service(zc, type_, name)

    def remove_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        """Callback which stops ttynvt and cleans up symlinks"""
        LOG.debug(f"Removing service: {name}")
        self.is_creating.acquire()
        self.is_creating = True

        service = self.name_map[name]

        port_name = service.properties.get(b"nick_name", name).decode("utf-8")
        link_name = self.get_link_name(port_name)
        if os.path.lexists(link_name):
            os.remove(link_name)
        self.links.remove(link_name)

        process, tcp_port = self.thread_map[port_name]
        self.available_ports.append(tcp_port)
        process.terminate()

        self.is_creating.release()

    def add_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        """Callback which creates a new device and connects to the rfc2217 server"""

        info = zc.get_service_info(type_, name)
        if info.properties.get(b"spec", None) == b"rfc2217":
            ser_name = info.properties.get(b"nick_name", name).decode("utf-8")

            # Looks which interface can be accessed the fastest
            ips = map(socket.inet_ntoa, info.addresses)
            hosts = multiping(
                ips,
                count=2,
                interval=0.1,
                timeout=0.5,
                concurrent_tasks=50,
                privileged=False,
            )
            min_rtt = 10000
            best_ip = None
            for host in hosts:
                if host.avg_rtt < min_rtt:
                    min_rtt = host.avg_rtt
                    best_ip = host.address

            self.name_map[name] = info
            self.thread_map[ser_name] = (
                self.ttynvt_thread(
                    ser_name,
                    info.port,
                    best_ip,
                ),
                info.port,
            )

    def cleanup(self):
        """Stops all ttynvt threads and removes the symlinks"""
        for process in self.thread_map.values():
            process.terminate()

        for link in self.links:
            os.remove(link)


def main():
    parser = argparse.ArgumentParser(
        prog=sys.argv[0],
        description="Create tty devices using ttynvt according to currently available zeroconf services",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Run in verbose mode",
        action="store_true",
        default=False,
    )

    args = parser.parse_args()

    if args.verbose:
        LOG_HANDLER.setLevel(logging.DEBUG)
        LOG.setLevel(logging.DEBUG)
    LOG.debug("Starting")
    zeroconf = Zeroconf()
    listener = ZeroconfService()
    browser = ServiceBrowser(zeroconf, "_serial-port._tcp.local.", listener)
    atexit.register(listener.cleanup)
    while True:
        sleep(1)
    zeroconf.close()


if __name__ == "__main__":
    main()
