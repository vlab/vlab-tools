import pyudev
import os.path
import socket


def generate_uid(port: pyudev.Device) -> str:
    """Generates the uid for a serial port"""
    usb_interface = None
    pci_id_str = ""
    for ancestor in port.ancestors:
        if ancestor.device_type == "usb_interface":
            usb_interface = os.path.split(ancestor.sys_path)[1]
        elif ancestor.subsystem == "pci":
            pci_id_str += os.path.split(ancestor.sys_path)[1]
    return f"{socket.gethostname()}-serial-{pci_id_str}-{usb_interface}".replace(
        ":", "_"
    )
