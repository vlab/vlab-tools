import pyudev
import os.path
import socket
import sys

from .common import generate_uid


def main():
    context = pyudev.Context()
    valid_ports = {}
    for port in context.list_devices(subsystem="tty"):
        if port.find_parent(subsystem="usb") != None:
            valid_ports[port.device_node] = generate_uid(port)

    if len(valid_ports.keys()) == 0:
        print("No USB<->Serial adapters found is it plugged in?")
        sys.exit(1)

    port_num_to_uid = {}
    for x, port in enumerate(valid_ports.keys()):
        print(f"{x}) {port}")
        port_num_to_uid[x] = valid_ports[port]

    while True:
        try:
            port_num = int(input("Enter port: "))
        except:
            print(f"{port_num} invalid")
            continue
        if port_num not in range(0, len(valid_ports.keys())):
            print(f"{port_num} invalid")
            continue
        break
    print(f"UID: {port_num_to_uid[port_num]}")


if __name__ == "__main__":
    main()
